const express = require('express')
const router = express.Router()

const appsController = require('../controllers/catalogController.js')

router.get('/all', appsController.list)

module.exports = router