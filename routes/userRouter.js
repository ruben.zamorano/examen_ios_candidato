const express = require('express')
const router = express.Router()

const appsController = require('../controllers/userController.js')

router.post('/login', appsController.login)

module.exports = router