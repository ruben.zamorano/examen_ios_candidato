const express = require('express')
const router = express.Router()

const appsController = require('../controllers/insidentController.js')

router.get('/all', appsController.list)
router.post('/delete', appsController.delete)
router.post('/create', appsController.create)
router.post('/editar', appsController.edit)
router.post('/getinsidentbyid', appsController.insidentDetail)

module.exports = router