const express = require('express')
const cors = require('cors')
const bodyParser = require('body-parser')
const app = express()

const userRouter = require('./routes/userRouter.js')
const insidentRouter = require('./routes/insidentRouter.js')
const catalogRouter = require('./routes/catalogRouter.js')


app.use(cors())
app.use(bodyParser.json())

app.use((req, res, next) => {
	let token = req.headers.authorization
	req.userId = token
	next()
})

app.use('/api/users', userRouter)
app.use('/api/insidents', insidentRouter)
app.use('/api/catalogs', catalogRouter)

app.listen(8001, function () {
	console.log('8001!')
})