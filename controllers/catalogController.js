const Catalog = require('../models/catalogModel.js')

const api = {}

api.list = (req, res) => {
	Catalog.findAll().then(catalog => {

		if (catalog) {
			return res.json({ respuesta: true, data: catalog })
		} else {
			return res.json({ respuesta: false, error: "Error al obtener los datos" })
		}

	})
}


module.exports = api