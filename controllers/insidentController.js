const zookeeper = require('zookeeper-cluster-client')
const Place = require('../models/insidentModel.js')



const api = {}

api.list = (req, res) => {
	Place.findAll().then(places => {

		if (places) {
			return res.json({ respuesta: true, data: places })
		} else {
			return res.json({ respuesta: false, error: "Error al obtener los datos" })
		}

	})
}


api.create = (req, res) => {


	if (!("cTitle" in req.body) || !("cLatitude" in req.body) || !("cLongitude" in req.body)) {

		return res.json({ respuesta: false, error: "Faltan datos" })
	}

	Place.create({title:req.body.cTitle, description:req.body.cDescription, latitud:req.body.cLatitude, longitud: req.body.cLongitude, hora:req.body.cHora, fecha: req.body.cFecha, tipo:req.body.cType }).then(place => {

		if (place) {

			return res.json({ respuesta: true, msj: "Lugar guardado" })

		} else {

			return res.json({ respuesta: false, error: "Error al guardar datos" })

		}

	})

}

api.edit = (req, res) => {

	if (!("cTitle" in req.body) || !("nId" in req.body) || !("cDescription" in req.body) ||!("cLatitude" in req.body) || !("cLongitude" in req.body)  || !("cHora" in req.body) || !("cFecha" in req.body) || !("cType" in req.body)) {

		return res.json({ respuesta: false, error: "Faltan datos", data:req.body })
	}

	Place.findOne({ where: { id: req.body.nId } }).then(place => {

		if (place) {

			place.description = req.body.cDescription
			place.title = req.body.cTitle
			place.tipo = req.body.cType
			place.latitud = req.body.cLatitude
			place.longitud = req.body.cLongitude
			place.hora = req.body.cHora
			place.fecha = req.body.cFecha

			place.save()
			return res.json({ respuesta: true, msj: "Datos editados" })
		} else {
			return res.json({ respuesta: false, error: "No se guardo Logo", codeError: "logoError" })
		}
	})
}
api.delete = (req, res) => {

	if (!("nId" in req.body)) {

		return res.json({ respuesta: false, error: "Faltan datos" })
	}
	Place.destroy({ where: { id: req.body.nId } }).then(place => {

		if (place) {

			return res.json({ respuesta: true, msj: "Lugar eliminado" })

		} else {

			return res.json({ respuesta: false, error: "No se pudo eliminar" })

		}

	})
}

api.insidentDetail = (req, res) => {

	if (!("nId" in req.body)) {

		return res.json({ respuesta: false, error: "Faltan datos" })
	}

	Place.findOne({where: {id:req.body.nId}}).then(place => {

		if (place){

		 return	res.json({ respuesta : true, data: place })
		}else{

		return	res.json({ respuesta : false, data: 'Habitacion no encontrada' })
		}

	})
}

module.exports = api