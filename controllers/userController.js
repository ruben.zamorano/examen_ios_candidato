const User = require('../models/userModel.js')

const api = {}

api.login = (req, res) => {

	let text = "";
	const possible = "abcdefghijklmnopqrstuvwxyz-ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
			  
	for (let i = 0; i < 51; i++){
		 text += possible.charAt(Math.floor(Math.random() * possible.length));

	}


	if (!('cEmail' in req.body) || !('cPassword' in req.body)) {
		return res.json({ error: 'Faltan parametros' })
	}

	User.findOne({ where: { email: req.body.cEmail, pass:req.body.cPassword } }).then(user => {
		if (user) {

			userRes = {
					id: user.id,
					email: user.email,
					token: text,
					type: user.type
				}

				user.token = text
				user.save()


				res.json({response:true, data:userRes})
			
		} else{
			res.json({response:false, error: 'Usuario/Contraseña Incorrecta' })
		}
	}).catch(err => {
		res.json({ response:false, error: 'Usuario/Contraseña Incorrecta' })
	})
}


module.exports = api