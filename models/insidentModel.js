const Sequelize = require('sequelize')
const sequelize = require('./config.js')

const Place = sequelize.define('insident', {
	
	id:{
		type: Sequelize.INTEGER,
		unique: true,
		index: true,
		primaryKey: true
	},
	title: {
		type: Sequelize.STRING
	},
	description: {
		type: Sequelize.STRING
	},
	latitud: {
		type: Sequelize.STRING
	},
	longitud: {
		type: Sequelize.STRING
	},
	hora: {
		type: Sequelize.STRING
	},
	fecha: {
		type: Sequelize.STRING
	},
	tipo: {
		type: Sequelize.INTEGER
	}
}, {
	timestamps: true
})

Place.sync({force: false}).then(() => {})
module.exports = Place