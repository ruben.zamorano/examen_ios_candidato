const Sequelize = require('sequelize')
const sequelize = require('./config.js')

const Catalog = sequelize.define('catalog', {
	
	id:{
		type: Sequelize.INTEGER,
		unique: true,
		index: true,
		primaryKey: true
	},
	title: {
		type: Sequelize.STRING
	}
}, {
	timestamps: true
})

Catalog.sync({force: false}).then(() => {})
module.exports = Catalog