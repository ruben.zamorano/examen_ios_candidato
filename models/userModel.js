const Sequelize = require('sequelize')
const sequelize = require('./config.js')

const User = sequelize.define('user', {
	
	id:{
		type: Sequelize.INTEGER,
		unique: true,
		index: true,
		primaryKey: true
	},
	token: {
		type: Sequelize.STRING
	},
	email: {
		type: Sequelize.STRING
	},
	type: {
		type: Sequelize.INTEGER
    },
    pass: {
		type: Sequelize.STRING
	}
}, {
	timestamps: true
})

User.sync({force: false}).then(() => {})
module.exports = User